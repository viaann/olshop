const historyArr = [];
let hasil = [];
let itemId;

const getItems = () => {
    fetch('./script/data.json')
        .then((response) => {
            return response.json();
        })
        .then((responseJson) => {
            renderItems(responseJson);
        })
        .catch((error) => {
            console.log(error);
        });
};

const getModal = (result, id) => {
    const modal = document.querySelector("#modal");
    itemId = id;
    for (let i = 0; i < result.length; i++) {
        if (result[i].id != id) {
            continue
        }
        modal.innerHTML = `<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header position-relative">
                    <img src="${result[i].img}" class="card-img-top" alt="...">
                    <button type="button" class="close position-absolute" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h3>${result[i].title}</h3>
                    <h6>${result[i].price}</h6>
                    <p>${result[i].desc}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="buy(hasil, itemId)">Buy</button>
                </div>
            </div>
        </div>
    </div>`
    }
}

function buy (result, id) {
  
       for (let i = 0; i < result.length; i++) {
        if (result[i].id != id) {
            continue
        }
       historyArr.push(result[i])
    } 
    showHistory()
    console.log(historyArr)
}

const showHistory = () => {
    const history = document.querySelector("#list");
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();
    today = mm + ' / ' + dd + ' / ' + yyyy 
    history.innerHTML = ""
    if (historyArr.length > 0) {
            historyArr.forEach(result => {
                history.innerHTML += `<div class="col-lg">
            <div class="text-center">
                <div class="card">
                    <img src="${result.img}" class="card-img-top history-img" alt="...">
                    <div class="card-body">
                        <div class="title-card">
                            <span style="float: left;
                            margin-left: 332px;
                            font-size: 12px;
                            color: #4CD3C2;"><b> ${result.title} </b></span><br>
                            <span class="card-price"><b> ${result.price} </b></span>
                            <span class="card-date"><b> Date: ${today}</b></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>`
            })
        console.log(historyArr)
        alert("Pembelian Berhasil")
    } else {
        history.innerHTML = `<div class="col-lg">
            <div class="text-center">
                <div class="card">
                    <div class="card-body">
                        <h4 style="text-align: center">No Item</h4>
                    </div>
                </div>
            </div>
        </div>`
    }
}

const renderItems = (results) => {
    const listPopular = document.querySelector("#listItems");
    listPopular.innerHTML = "";
    results.forEach(result => {
        hasil.push(result)
        listPopular.innerHTML += `
        <div class="col-md-3">
                    <div class="text-center">
                        <a id="${result.id}" onclick="getModal(hasil, id);" data-toggle="modal" data-target="#staticBackdrop" >
                            <div class="card">
                                <img id="img" src="${result.img}" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <span class="card-title"><b> ${result.title} </b></span><br>
                                    <a href="#"> <b> ${result.price}</b></a>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
         `;
    });
};


showHistory();
getItems();
